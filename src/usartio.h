/**
   emma_project
    Copyright (C) 2015  Rodrigo Garcia

 This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef _USARTIO_H_

#define _USARTIO_H_

#include "usart.h"
#include <stdio.h>

int usart_putchar(char c, FILE *stream);

int usart_getchar(FILE *stream);

// ********** stdio.h declarations ****************

/* printf and scanf is included (too much mem) */
//FILE usart=FDEV_SETUP_STREAM(usart_putchar,usart_getchar,_FDEV_SETUP_RW); 

/* printf is included */
//FILE usart=FDEV_SETUP_STREAM(usart_putchar,NULL,_FDEV_SETUP_WRITE);
// ************************************************

void usart_printf_init(void);

#endif
/* ---------------------------- */


/* FILE usart=FDEV_SETUP_STREAM(usart_putchar,usart_getchar,_FDEV_SETUP_RW); */

/* void usart_io(void){ */
/* 	usart_init(); */
/* 	stdout=&usart; */
/* 	stdin=&usart; */
/* } */
