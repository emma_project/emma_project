/**
 emma_project
 Copyright (C) 2015  Rodrigo Garcia

 This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

/*
 GENERIC SD CARD HEADERS

 The purpouse of this header is to define the essential SD card
 functions to be used in the system.

 It is a sort of template to keep calls to SD card functions 
 independient of the library used.

 */

#ifndef _SD_GENERIC_H_

#define _SD_GENERIC_H_

#include "SPI_routines.h"
#include "SD_routines.h"
#include "FAT32.h"

unsigned char SD_initialization();

void SD_list_files();

unsigned char SD_readFile(char* filename);

unsigned char SD_writeFile(char* filename);

unsigned char SD_deleteFile(char* filename);

#endif
