#include <avr/io.h>
#include "pff.h"

BYTE xmit_spi(BYTE c) {
	SPDR = c;
	while (!SPCR & (1<<SPIF));
	return SPDR;
}

BYTE rcv_spi(void) {
	return xmit_spi(0xFF);
}
