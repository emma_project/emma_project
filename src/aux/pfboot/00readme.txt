Petit FatFs Module Sample Projects                             (C)ChaN, 2010


DIRECTORIES

  <generic> Generic microcontrollers with MMC
  <avr>     ATMEL AVR (ATtiny85,8-bit,RISC) with MMC
  <win32>   Windows (VC++ 6.0)

  These are sample projects for function/compatibility test of FatFs module
  with low level disk I/O codes.



AGREEMENTS

  These sample projects for Petit FatFs module are free software and there is
  NO WARRANTY. You can use, modify and redistribute it for personal, non-profit
  or commercial use without any restriction UNDER YOUR RESPONSIBILITY.



REVISION HISTORY

  Jun 15, 2009  First release. (branched from FatFs R0.07b)
  Dec 14, 2009  Modified for R0.02
  Oct 23, 2010  Added a sample of generic uC

