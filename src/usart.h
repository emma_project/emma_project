/**
    Copyright (C) 2013  Rodrigo Garcia

 This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
//#include "usart.c" // added
#ifndef _USART_H_
#define _USART_H_

void usart_init(void);
void usart_baud_38400(void);
void usart_baud_9600(void);

#define tx_ready() ( UCSRA & (1<<UDRE))

void usart_tx(char data);

#define rx_ready() (UCSRA & (1<<RXC))

char usart_rx(void);

char usart_rx_1s(void);

#endif
