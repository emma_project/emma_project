/**
   EMMA = Estación Meteorológica de Muestreo Automática
 
 emma_project
 Copyright (C) 2015  Rodrigo Garcia

 This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

------------ Build ------------
   See 'Makefile' inside Build

*/

#include <avr/io.h>
#include <avr/pgmspace.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include "usartio.h"
#include "SD_generic.h"

// Coment the following line to disable debug
#define DEBUG_MODE
// Note.- previous functionality will be chosen by a switch in the future

// ports initialization
void init_ports(void)
{ // see if it is ok
  PORTA = 0x00;
  DDRA = 0xFF;
  PORTB = 0xEF;
  DDRB  = 0xBF; //MISO line i/p, rest o/p 
  PORTC = 0x00;
  DDRC  = 0xFF;
  PORTD = 0x00;
  DDRD  = 0xFE;
}

// routine to init external devices
void init_devices(void)
{
  cli(); // interupts disabled
  init_ports();
  
  // -- perpipherals initialization --
  // usart
  usart_printf_init(); //printf is initialized via usart
  // spi
  spi_init();
  // registers
  MCUCR = 0x00;
  GICR  = 0x00;
  TIMSK = 0x00; //timer interrupt sources
  // ----
}


// -------------------------- Main program -----------------------
int main(void)
{
  _delay_ms(100);  //delay for VCC stabilization

  PORTD |= 1<<2; //switching ON the LED (for testing purpose only)

  init_devices();

  PORTD &= ~(1<<2); //switching OFF the LED (test only)
  _delay_ms(444); // addednn
  PORTD |= 1<<2; //switching ON the LED (for testing purpose only)

  // Normal operation
  // TODO: add sensors to report samples in files


  /* ***************** DEBUG_MODE ***************** */
  /* In this mode individual divieces can be tested by giving commands */
  /* via usart and the micro will answer results via usart too */
  /*   In the future a switch will put the system in debug mode, awaiting */
  /* queries from outside */
#ifdef DEBUG_MODE

  unsigned char option, error, data, FAT32_active;
  unsigned int i;
  unsigned char fileName[13];

  char cc='a';
  while (cc!='q') // to test the Usart comunication
  {
    cc = usart_rx_1s();
    printf("\npress q to continue %c",cc);
    printf_P(PSTR(" .."));
  }

  printf_P(PSTR("\nStart!\n"));

  cardType = 0;

  // --- Testing the SD card initialization ----

  error = SD_initialization();

  if(error)
    {
      if(error == 1) printf_P(PSTR("SD card not detected.."));
      if(error == 2) printf_P(PSTR("Card Init failed.."));

      while(1);  //wait here forever if error in SD init 
    }

  switch (cardType)
    {
    case 1:printf_P(PSTR("Std Cap Card(Ver 1.x)!"));
      break;
    case 2:printf_P(PSTR("H Cap Card!"));
      break;
    case 3:printf_P(PSTR("Std Cap Card(Ver 2.x)!"));
      break;
    default:printf_P(PSTR("Ukwn SD Card!"));
      break; 
    }


  SPI_HIGH_SPEED;	//SCK - 4 MHz
  _delay_ms(1);   //some delay

  FAT32_active = 1;
  error = getBootSectorData (); //read boot sector and keep necessary data in global variables
  if(error) 	
    {
      printf_P(PSTR("\nFAT32 not found!"));  //FAT32 incompatible drive
      FAT32_active = 0;
    }

  while(1)
    {
      printf_P(PSTR("Press key\n"));
      option = usart_rx();
      printf_P(PSTR("\n>0:Erase Blocks"));
      printf_P(PSTR("\n>1:Write single Block"));
      printf_P(PSTR("\n>2:Read single Block"));

#ifndef FAT_TESTING_ONLY
      
      printf_P(PSTR("\n>3:Write multiple Blocks"));
      
      printf_P(PSTR("\n>4:Read multiple Blocks"));
#endif
      
      printf_P(PSTR("\n>5:Get file list"));
      
      printf_P(PSTR("\n>6:Read File"));
      
      printf_P(PSTR("\n>7:Write File"));
      
      printf_P(PSTR("\n>8:Delete File"));
      
      printf_P(PSTR("\n>9:Read SD Memory Capacity (Total/Free)"));

      printf_P(PSTR("\n>Select Option (0-9):"));


      /*WARNING: If option 0, 1 or 3 is selected, the card may not be detected by PC/Laptop again,
	as it disturbs the FAT format, and you may have to format it again with FAT32.
	This options are given for learnig the raw data transfer to & from the SD Card*/

      option = usart_rx();
      usart_tx(option);

      if(option >=0x35 && option <=0x39)  //options 5 to 9 disabled if FAT32 not found
	{
	  if(!FAT32_active) 
	    {
	      
	      printf_P(PSTR("\nFAT32 options disabled!"));
	      continue;
	    } 
	}


      if((option >= 0x30) && (option <=0x34)) //get starting block address for options 0 to 4
	{
	  
	  
	  printf_P(PSTR("\nEnter the Block number(0000-9999):"));
	  data = usart_rx(); usart_tx(data);
	  startBlock = (data & 0x0f) * 1000;
	  data = usart_rx(); usart_tx(data);
	  startBlock += (data & 0x0f) * 100;
	  data = usart_rx(); usart_tx(data);
	  startBlock += (data & 0x0f) * 10;
	  data = usart_rx(); usart_tx(data);
	  startBlock += (data & 0x0f);
	  
	}

      totalBlocks = 1;

#ifndef FAT_TESTING_ONLY

      if((option == 0x30) || (option == 0x33) || (option == 0x34)) //get total number of blocks for options 0, 3 or 4
	{
	  
	  
	  printf_P(PSTR("\nHow many blocks?(000-999):"));
	  data = usart_rx(); usart_tx(data);
	  totalBlocks = (data & 0x0f) * 100;
	  data = usart_rx(); usart_tx(data);
	  totalBlocks += (data & 0x0f) * 10;
	  data = usart_rx(); usart_tx(data);
	  totalBlocks += (data & 0x0f);
	  
	}
#endif

      switch (option)
	{
	case '0': //error = SD_erase (block, totalBlocks);
          error = SD_erase (startBlock, totalBlocks);
          
          if(error)
	    printf_P(PSTR("\nErase failed.."));
          else
	    printf_P(PSTR("\nErased!"));
          break;

	case '1': 
          printf_P(PSTR("\nEnter text(End with~);"));
          i=0;
	  do
            {
	      data = usart_rx();
	      usart_tx(data);
	      buffer[i++] = data;
	      if(data == 0x0d)
                {
		  usart_tx(0x0a);
		  buffer[i++] = 0x0a;
                }
	      if(i == 512) break;
            }while (data != '~');

	  error = SD_writeSingleBlock (startBlock);
	  
	  
	  if(error)
	    printf_P(PSTR("\nWrite failed!"));
	  else
	    printf_P(PSTR("\nWrite successful!"));
	  break;

	case '2': error = SD_readSingleBlock (startBlock);
          
          if(error)
            printf_P(PSTR("\nRead failed!"));
          else
	    {
	      for(i=0;i<512;i++)
		{
		  if(buffer[i] == '~') break;
		  usart_tx(buffer[i]);
		}
	      
	      
	      printf_P(PSTR("\nRead successful!"));
	    }

          break;
	  //next two options will work only if following macro is cleared from SD_routines.h
#ifndef FAT_TESTING_ONLY

	case '3': 
          error = SD_writeMultipleBlock (startBlock, totalBlocks);
          
          if(error)
            printf_P(PSTR("\nWrite failed!"));
          else
            printf_P(PSTR("\nWrite successful!"));
          break;

	case '4': error = SD_readMultipleBlock (startBlock, totalBlocks);
          
          if(error)
            printf_P(PSTR("\nRead failed!"));
          else
            printf_P(PSTR("\nRead successful!"));
          break;
#endif

	case '5': 
	  SD_list_files();
	  //	  findFiles(GET_LIST,0);
          break;

	case '6': 
	case '7': 
	case '8': 
	  
          printf_P(PSTR("\nEnter file name:"));
          for(i=0; i<13; i++)
	    fileName[i] = 0x00;   //clearing any previously stored file name
          i=0;
          while(1)
	    {
	      data = usart_rx();
	      if(data == 0x0d) break;  //'ENTER' key pressed
	      if(data == 0x08)	//'Back Space' key pressed
		{ 
		  if(i != 0)
		    { 
		      usart_tx(data);
		      usart_tx(' '); 
		      usart_tx(data); 
		      i--; 
		    } 
		  continue;     
		}
	      if(data <0x20 || data > 0x7e) continue;  //check for valid English text character
	      usart_tx(data);
	      fileName[i++] = data;
	      if(i==13)
	      {
		printf_P(PSTR("\nfile name too long.."));
		break;
	      }
	    }
          if(i>12) break;
       
	  
	  if(option == '6')
	    SD_readFile(fileName);
	    //	    readFile( READ, fileName);
	  if(option == '7')
	    SD_writeFile(fileName);
	  //writeFile(fileName);
	  if(option == '8')
	    SD_deleteFile(fileName);
	  //	    deleteFile(fileName);
          break;

	case '9': memoryStatistics();
          break;
	default: 
	  printf_P(PSTR("\nInvalid option"));

	}
/* ******************************************** */      
    }
#endif
  return 0;
}


// ---------------------------------------------------------------
