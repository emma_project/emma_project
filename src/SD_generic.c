/**
 emma_project
 Copyright (C) 2015  Rodrigo Garcia

 This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*

  SD GENERIC SOURCE FILE
 
  the purpouse of this file is to make easier the use of the SD
  card functions from included libraries in the header.
  To do so the following functions use the FAT and SD libraries
  to perform essential operations on the SD card and data storage.
  
  The main file should worry only on calling this file's funcitons
  to perform SD card funcitons, that's why when other SD, SPI
  or FAT library is used, it is mandatory to do changes here first.

*/

#include "SD_generic.h"

unsigned char SD_initialization()
{
  // It assumes spi module has been initializated
  unsigned char i, Error;
  for (i=10; i>0; i--)
    {
      Error = SD_init();
      if(!Error)
	return 0;
    }

  return Error;
}

void SD_list_files()
{
  findFiles(GET_LIST, 0);
  return 0;
}

unsigned char SD_readFile(char* filename)
{
  readFile(READ, filename);
  return 0;
}

unsigned char SD_writeFile(char* filename)
{
  writeFile(filename);
  return 0;
}

unsigned char SD_deleteFile(char* filename)
{
  deleteFile(filename);
  return 0;
}
